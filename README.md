Check for pending settings that require a reboot through the REST API in Xclarity Controllers

GET request https://ip:443/redfish/v1/Systems/1/Bios 

This will show the current settings, and also a short Messages array which shows that changes have occurred.

Example Current Setting: "Processors_HyperThreading": "Enable",

The above request will not show the pending value for the Processors_HyperThreading attribute. In order to see the pending value the following request is needed:

GET request https://ip:443/redfish/v1/Systems/1/Bios/Pending 

Example Pending Setting:  "Processors_HyperThreading": "Disable",

Upon rebooting the system, the  https://ip:443/redfish/v1/Systems/1/Bios will now return
Example Current Setting:  "Processors_HyperThreading": "Disable",